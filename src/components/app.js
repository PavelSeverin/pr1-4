import React from 'react';
import './app.css';
import { PostForm } from './post-form';
import { Post } from './post';

export default class App extends React.Component {
  state = {
    isStyledToggled: false,
    searchValue: '',
    searchBy: undefined,
    posts: [
      {
        title: 'React',
        text: 'JavaScript-библиотека для создания пользовательских интерфейсов',
        id: 1,
      },
      {
        title: 'Angular',
        text:
          'Angular is an app-design framework and development platform for creating efficient and sophisticated single-page apps.',
        id: 2,
      },
      {
        title: 'Vue',
        text:
          'Vue (pronounced /vjuː/, like view) is a progressive framework for building user interfaces',
        id: 3,
      },
      {
        title: 'Node.js',
        text:
          'Node.js® — це JavaScript–оточення побудоване на JavaScript–рушієві Chrome V8.',
        id: 4,
      },
    ],
  };
  postTitleRef = React.createRef();
  postTextRef = React.createRef();

  updatePostIds = (posts) => {
    return posts.map((post, i) => ({
      ...post,
      id: i + 1,
    }));
  };

  addPost = (newPost) => {
    this.setState((prevState) => ({
      posts: this.updatePostIds([newPost, ...prevState.posts]),
    }));
  };

  deletePost = (postIdToDelete) => {
    this.setState((prevState) => {
      const nextPosts = prevState.posts.filter(
        ({ id }) => id !== postIdToDelete
      );
      return { posts: this.updatePostIds(nextPosts) };
    });
  };

  toggleStyles = () => {
    this.setState((prev) => {
      if (prev.isStyledToggled) {
        this.postTitleRef.current.style.color = 'red';
        this.postTextRef.current.style.fontWeight = 'bold';
      } else {
        this.postTitleRef.current.style.color = 'black';
        this.postTextRef.current.style.fontWeight = 'normal';
      }
      return { isStyledToggled: !prev.isStyledToggled };
    });
  };

  get postsToShow() {
    const { searchValue, posts, searchBy } = this.state;
    if (!searchValue.trim() || !searchBy) {
      return posts;
    }

    return posts.filter((post) => {
      const val = post[searchBy];
      return val.toLowerCase().includes(searchValue.toLowerCase());
    });
  }

  applySearch = (searchBy) => {
    this.setState({ searchBy });
  };

  onSearchChange = (searchValue) => {
    this.setState({ searchBy: undefined, searchValue });
  };

  render() {
    const { searchValue } = this.state;
    return (
      <div>
        <h1>React components</h1>
        <PostForm
          addPost={this.addPost}
          toggleStyles={this.toggleStyles}
          postTitleRef={this.postTitleRef}
          postTextRef={this.postTextRef}
          searchValue={searchValue}
          applySearch={this.applySearch}
          onSearchChange={this.onSearchChange}
        />
        {this.postsToShow.map((post) => (
          <Post key={post.id} post={post} deletePost={this.deletePost} />
        ))}
      </div>
    );
  }
}
