import React, { Component } from 'react';

export class PostForm extends Component {
  state = {
    title: '',
    text: '',
  };

  onChange = (name) => (ev) => this.setState({ [name]: ev.target.value });

  onSearchChange = (ev) => {
    const search = ev.target.value;
    this.setState({ search });
    this.props.applySearch(search);
  };

  onAddPost = () => {
    const { addPost } = this.props;
    const { title, text } = this.state;
    addPost({ title, text });
  };

  onSearchChange = (ev) => {
    this.props.onSearchChange(ev.target.value);
  };

  render() {
    const { title, text } = this.state;
    const {
      toggleStyles,
      postTitleRef,
      postTextRef,
      applySearch,
      searchValue,
    } = this.props;
    return (
      <div>
        <input
          ref={postTitleRef}
          className="new-post-title"
          type="text"
          placeholder="Title"
          onChange={this.onChange('title')}
          value={title}
        />
        <br />
        <br />
        <textarea
          ref={postTextRef}
          className="new-post-area"
          placeholder="Text"
          rows={5}
          onChange={this.onChange('text')}
          value={text}
        />
        <br />
        <button onClick={this.onAddPost}>Add Post!</button>
        <button onClick={toggleStyles}>Toggle Styles</button>
        <hr />
        <h2>Search posts!</h2>
        <input
          type="text"
          placeholder="Search..."
          value={searchValue}
          onChange={this.onSearchChange}
        />
        <div>
          <button onClick={() => applySearch('title')}>Search by title</button>
          <button onClick={() => applySearch('text')}>Search by text</button>
        </div>
      </div>
    );
  }
}
